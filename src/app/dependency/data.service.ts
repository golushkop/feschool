import {Injectable} from '@angular/core';

const data: Item[] = [{id: 'product1'}, {id: 'product2'}];
interface Item {
  id: string;
}
@Injectable()
export class DataService {
  counter = 0;

  private data: Item[] = data;

  constructor() {
  }

  getData(): Item[] {
    return this.data;
  }

  setData(item: Item): void {
    this.data.push(item);
  }
}
