import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DependencyRoutingModule} from './dependency-routing.module';
import {DependencyComponent} from './dependency.component';
import {TestComponent} from './test/test.component';
import {Test2Component} from './test2/test2.component';

@NgModule({
  declarations: [
    DependencyComponent,
    TestComponent,
    Test2Component
  ],
  imports: [
    CommonModule,
    DependencyRoutingModule
  ],
  providers: []
})
export class DependencyModule {
}
