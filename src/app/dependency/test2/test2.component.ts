import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.scss']
})
export class Test2Component implements OnInit {

  constructor(private dataService: DataService) {
  }

  get counterValue(): number {
    return this.dataService.counter;
  }

  get data() {
    return JSON.stringify(this.dataService.getData());
  }

  ngOnInit() {
  }

  handleClick() {
    this.dataService.counter++;
    const id = (Math.floor(Math.random() * 10)).toString();
    this.dataService.setData({id});
  }

}
