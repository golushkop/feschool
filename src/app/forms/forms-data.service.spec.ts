import { TestBed } from '@angular/core/testing';

import { FormsDataService } from './forms-data.service';

describe('FormsDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormsDataService = TestBed.get(FormsDataService);
    expect(service).toBeTruthy();
  });
});
