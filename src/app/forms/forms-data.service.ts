import {Injectable} from '@angular/core';

export interface User {
  firstName: string;
  lastName: string;
  gender: 'Mr' | 'Ms';
  city: string;
}

@Injectable({
  providedIn: 'root'
})
export class FormsDataService {

  private users: User[] = [];

  private userGenders: string[] = ['Mr', 'Ms'];

  constructor() {
  }

  registerUser(user: User) {
    this.users.push(user);
  }

  getUsers(): User[] {
    return this.users;
  }

  getUserGenders(): string[] {
    return this.userGenders;
  }
}
