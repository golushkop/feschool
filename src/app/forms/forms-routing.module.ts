import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormsComponent} from './forms.component';
import {TemplateDrivenComponent} from './template-driven/template-driven.component';
import {ReactiveComponent} from './reactive/reactive.component';

const routes: Routes = [
  {
    path: '',
    component: FormsComponent,
    children: [
      {
        path: 'template',
        component: TemplateDrivenComponent
      },
      {
        path: 'reactive',
        component: ReactiveComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
