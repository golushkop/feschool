import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsComponent} from './forms.component';
import {TemplateDrivenComponent} from './template-driven/template-driven.component';
import {FormsRoutingModule} from './forms-routing.module';
import {ReactiveComponent} from './reactive/reactive.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [TemplateDrivenComponent, FormsComponent, ReactiveComponent],
  providers: [],
  imports: [
    CommonModule,
    FormsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class FormsTestModule {
}
