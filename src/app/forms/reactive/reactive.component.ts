import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormsDataService, User} from '../forms-data.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss']
})
export class ReactiveComponent implements OnInit {
  private defaultUser: User = {gender: 'Mr', firstName: '', lastName: '', city: ''};
  formGroup: FormGroup;

  constructor(private formsService: FormsDataService, private fb: FormBuilder) { }

  ngOnInit() {
    // this.formGroup = new FormGroup({
    //   firstName: new FormControl(''),
    //   lastName: new FormControl(''),
    //   city: new FormControl(''),
    //   gender: new FormControl('Mr'),
    // });
    this.formGroup = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: ['Mr', Validators.required],
      city: ['', Validators.required],
    });
    console.log(this.formGroup);
    this.formGroup.valueChanges.subscribe(console.log);
  }

  get userGenders(): string[] {
    return this.formsService.getUserGenders();
  }

  get dataString(): string {
    return JSON.stringify(this.formGroup.getRawValue());
  }

  get allUsers(): string {
    return JSON.stringify(this.formsService.getUsers());
  }


  handleSubmit(): void {
    this.formsService.registerUser(this.formGroup.getRawValue());
    this.formGroup.reset({...this.defaultUser});
  }

}
