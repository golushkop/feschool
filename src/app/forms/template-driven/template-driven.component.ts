import {Component, OnInit} from '@angular/core';
import {FormsDataService, User} from '../forms-data.service';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.scss']
})
export class TemplateDrivenComponent implements OnInit {
  private defaultUser: User = {gender: 'Mr', firstName: '', lastName: '', city: ''};
  user: User = {...this.defaultUser};

  constructor(private formsService: FormsDataService) {
  }

  get userGenders(): string[] {
    return this.formsService.getUserGenders();
  }

  get dataString(): string {
    return JSON.stringify(this.user);
  }

  get allUsers(): string {
    return JSON.stringify(this.formsService.getUsers());
  }

  ngOnInit() {
  }

  handleSubmit(): void {
    this.formsService.registerUser(this.user);
    this.user = {...this.defaultUser};
  }

}
